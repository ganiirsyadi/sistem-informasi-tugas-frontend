import axios from "axios"
import { BE_BASE_URL, FE_BASE_URL } from "../constant"
import { getToken } from "../helper"

export class AuthenticationAPI {
  async signUp(data) {
    return axios.post(`${BE_BASE_URL}/user/signup`, data)
  }

  async login(data) {
    return axios.post(`${BE_BASE_URL}/user/login`, data)
  }

  async getUser() {
    return axios.get(`${BE_BASE_URL}/user/`,  {
      headers: { Authorization: "Bearer " + getToken() },
    })
  }
}
