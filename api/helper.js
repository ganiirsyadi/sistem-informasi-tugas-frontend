import { FE_BASE_URL } from "./constant"

export const saveToken = (token) => {
  window.localStorage.setItem("token", token)
}

export const getToken = () => {
  const token = window.localStorage.getItem("token")
  
  if (token) return token
  else redirectToLogin()
}

export const deleteToken = () => {
  window.localStorage.removeItem("token")
}

export const redirectToLogin = () => window.location.replace(`${FE_BASE_URL}/login`);

export const redirect = (path) => window.location.replace(`${FE_BASE_URL}${path}`);