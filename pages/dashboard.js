import react, { Component } from "react";
import {
  Center,
  VStack,
  Grid,
  Heading,
  Button,
  Flex,
  Spinner,
} from "@chakra-ui/react";
import { Navbar } from "../components/Navbar";
import { Footer } from "../components/Footer";
import { BoxTugas } from "../components/BoxTugas";
import Link from "next/link";
import { TugasAPI } from "../api/endpoints/tugas";
import React, { useEffect, useState } from "react";
import { redirect } from "../api/helper";

const Dashboard = (props) => {
  const tugasAPI = new TugasAPI();
  const [tugas, setTugas] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    tugasAPI
      .getAllTugas()
      .then((res) => {
        setTugas(res.data);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false)
        if (err.response.status === 403) redirect("/login")
      });
  }, []);

  return (
    <>
      <Navbar />
      <Center backgroundColor="teal.400" minHeight="100vh" pt="5rem" pb="5rem">
        <VStack w={{ xl: "24rem" }}>
          <VStack w="100%">
            <VStack mb={4} w="100%">
              <Heading mb={4} textAlign="left" color="blue.900" fontSize="4xl">
                Daftar Tugas
              </Heading>
              <Link href="/edit-matkul">
                <Button backgroundColor="blue.900" color="white">
                  Edit Mata Kuliah
                </Button>
              </Link>
            </VStack>
          </VStack>
          {loading ? (
            <Flex justifyContent="center">
              <Spinner size="xl" />
            </Flex>
          ) : (
            <Grid templateColumns="repeat(3, 1fr)" gap={9}>
              {tugas.map((element) => (
                <BoxTugas
                  key={element.id}
                  id={element.id}
                  judul={element.judul}
                  matkul={element.matkul}
                  deadline={element.deadline}
                />
              ))}
            </Grid>
          )}

          <Flex justifyContent="center" pt="9">
            <Link href="/create/tugas">
              <Button backgroundColor="blue.900" color="white">
                Add Tugas
              </Button>
            </Link>
          </Flex>
        </VStack>
      </Center>
      <Footer />
    </>
  );
};

export default Dashboard;
