import {
  Center,
  VStack,
  Input,
  Heading,
  Flex,
  Button,
  Checkbox,
  Text,
  Box,
  FormControl,
  FormLabel,
  HStack,
  Grid,
  Divider,
  Spinner,
} from "@chakra-ui/react";
import { Navbar } from "../../components/Navbar";
import { Footer } from "../../components/Footer";
import Head from "next/head";
import { BoxTugas } from "../../components/BoxTugas";
import { useRouter } from "next/router";
import { TugasAPI } from "../../api/endpoints/tugas";
import { useEffect, useState } from "react";
import { KomentarAPI } from "../../api/endpoints/komentar";
import { redirect, redirectToLogin } from "../../api/helper";

const formatDate = (dateString) => {
  const options = { year: "numeric", month: "long", day: "numeric" };
  return new Date(dateString).toLocaleDateString(undefined, options);
};

const isExpired = (date) => {
  const now = new Date();
  const deadline = new Date(date);
  if (deadline > now) return false;
  return true;
};

export default function Tugas(props) {
  const router = useRouter();
  const tugasAPI = new TugasAPI();
  const komentarAPI = new KomentarAPI();
  const [tugas, setTugas] = useState({});
  const [comment, setComment] = useState("");
  const [loading, setLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(true);

  const onSubmit = () => {
    setLoading(true);
    if (router.isReady) {
      komentarAPI
        .createKomentar(router.query.id, { comment: comment })
        .then((res) => {
          setLoading(false);
        })
        .catch((err) => { });
    }
  };

  useEffect(() => {
    if (router.isReady) {
      setPageLoading(true);
      tugasAPI
        .getDetailTugas(router.query.id)
        .then((res) => {
          console.log(res);
          setTugas(res.data);
          setPageLoading(false);
        })
        .catch((err) => {
          if (err.response.status === 403) redirectToLogin();
        });
    }
  }, [router.isReady]);

  useEffect(() => {
    if (router.isReady) {
      tugasAPI
        .getDetailTugas(router.query.id)
        .then((res) => {
          console.log(res);
          setTugas(res.data);
        })
        .catch((err) => {
          if (err.response.status === 403) redirectToLogin();
        });
    }
  }, [loading]);

  return (
    <>
      <Navbar />
      <Center backgroundColor="teal.400" minH="100vh" pt="14">
        {pageLoading ? (
          <Spinner size="xl" />
        ) : (
          <HStack alignItems="flex-start" spacing="2">
            <VStack spacing="2" width="400px">
              <Box
                backgroundColor="teal.50"
                borderRadius="10px"
                p="6"
                width="100%"
              >
                <Heading fontSize="2xl" mb="2">
                  {tugas.judul}
                </Heading>
                <Text mb="4">{tugas.matkul}</Text>
                <Box
                  mb="4"
                  backgroundColor={`${isExpired(tugas.deadline) ? "red.600" : "teal.600"
                    }`}
                  borderRadius="5px"
                  px="2"
                  py="1"
                  color="white"
                  fontSize="medium"
                  width="fit-content"
                >
                  {formatDate(new Date(tugas.deadline))}
                </Box>
                <Text mb="4">{tugas.deskripsi}</Text>
                <Flex width="100%" justifyContent="end">
                  <a href={"https://www." + tugas.link} target="_blank">
                    <Button backgroundColor="blue.900" color="white">
                      Visit Link
                    </Button>
                  </a>
                </Flex>
              </Box>

              {new Date(tugas.deadline) > new Date() && (
                <Box
                  backgroundColor="teal.50"
                  borderRadius="10px"
                  p="6"
                  width="100%"
                >
                  <Heading mb="4" fontSize="xl">
                    Add Comment
                  </Heading>
                  <Grid templateColumns="75% 25%" columnGap="2">
                    <Input
                      mb="4"
                      placeholder="New Comment"
                      backgroundColor="white"
                      value={comment}
                      onChange={(e) => setComment(e.target.value)}
                    />
                    <Button
                      backgroundColor="blue.900"
                      color="white"
                      onClick={() => onSubmit()}
                    >
                      Submit
                    </Button>
                  </Grid>
                </Box>
              )}
            </VStack>
            <Box
              backgroundColor="teal.50"
              borderRadius="10px"
              p="6"
              width="400px"
            >
              <Heading mb="4" fontSize="xl">
                Comments
              </Heading>

              <VStack
                maxHeight="calc(100vh - 300px)"
                overflowY="auto"
                align="flex-start"
              >
                {loading ? (
                  <Box
                    borderRadius="10px"
                    backgroundColor="white"
                    p="2"
                    mb="2"
                    w="100%"
                  >
                    <Text> Loading </Text>
                  </Box>
                ) : (
                  tugas?.komentar?.map((element) => (
                    <Box
                      borderRadius="10px"
                      backgroundColor="white"
                      p="2"
                      mb="2"
                      key={element.id}
                      w="100%"
                    >
                      <Text fontSize="medium" mb="2" fontWeight="bold">
                        from: {element.author}
                      </Text>
                      <Text> {element.comment} </Text>
                    </Box>
                  ))
                )}
              </VStack>
            </Box>
          </HStack>
        )}
      </Center>
      <Footer />
    </>
  );
}
