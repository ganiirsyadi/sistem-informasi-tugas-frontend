import React, { useEffect, useState } from "react";
import { MatkulAPI } from "../../api/endpoints/matkul";
import { redirect } from "../../api/helper";
import { Navbar } from "../../components/Navbar";
import { Footer } from "../../components/Footer";
import {
  VStack,
  Center,
  Heading,
  Select,
  FormControl,
  FormLabel,
  Input,
  Button,
  useToast,
  Spinner
} from "@chakra-ui/react";
import { TugasAPI } from "../../api/endpoints/tugas";

const Tugas = () => {
  const toast = useToast()
  const matkulApi = new MatkulAPI();
  const tugasApi = new TugasAPI();
  const [listMatkul, setListMatkul] = useState([]);
  const [matkul, setMatkul] = useState("");
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState({})
  // const [judul, setJudul] = useState("")
  // const [deskripsi, setDeskripsi] = useState("")
  // const [link, setLink] = useState("")
  // const [deadline, setDeadline] = useState(new Date())

  useEffect(() => {
    matkulApi
      .getAllMatkul()
      .then((res) => setListMatkul(res.data))
      .catch((err) => {
        if (err.response.status === 403) redirect("/login");
      });
  }, []);

  const submitHandler = () => {
    setLoading(true)
    tugasApi
      .createTugas(matkul, data)
      .then((res) => {
        setLoading(false)
        toast({
          title: "Tugas created.",
          description:
            "All the subscribers has received a notification from their email",
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        console.log(err)
        toast({
          title: "Failed to create tugas",
          status: "failed",
          duration: 9000,
          isClosable: true,
        });
        setLoading(false)
      });
  };

  return (
    <>
      <Navbar />
      <Center backgroundColor="teal.400" minHeight="100vh" pt="12">
        <VStack spacing="4">
          <Heading mb="4">Add Tugas</Heading>
          <Select
            placeholder="Select Matkul"
            bg="white"
            value={matkul}
            onChange={(e) => setMatkul(e.target.value)}
          >
            {listMatkul.length > 0 &&
              listMatkul.map((el) => (
                <option key={el.kodeMatkul} value={el.kodeMatkul}>
                  {el.nama}
                </option>
              ))}
          </Select>
          <FormControl>
            <FormLabel>Judul</FormLabel>
            <Input
              isRequired
              value={data.judul}
              onChange={(e) => {
                setData({ ...data, judul: e.target.value });
              }}
              bg="white"
              type="text"
            />
          </FormControl>
          <FormControl>
            <FormLabel>Deskripsi</FormLabel>
            <Input
              isRequired
              value={data.deskripsi}
              onChange={(e) => {
                setData({ ...data, deskripsi: e.target.value });
              }}
              bg="white"
              type="text"
            />
          </FormControl>
          <FormControl>
            <FormLabel>Link</FormLabel>
            <Input
              value={data.link}
              onChange={(e) => {
                setData({ ...data, link: e.target.value });
              }}
              isRequired
              bg="white"
              type="text"
            />
          </FormControl>
          <FormControl>
            <FormLabel>Deadline</FormLabel>
            <Input
              value={data.deadline}
              onChange={(e) => {
                setData({ ...data, deadline: e.target.value });
              }}
              isRequired
              bg="white"
              type="date"
            />
          </FormControl>
          <Button onClick={() => submitHandler()} bg="blue.600" color="white">
            {loading ? <Spinner size="xl" /> : "Add Tugas"}
          </Button>
        </VStack>
      </Center>
      <Footer />
    </>
  );
};

export default Tugas;
