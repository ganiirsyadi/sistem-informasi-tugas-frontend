import React, { useState } from "react";
import {
  Center,
  Heading,
  FormControl,
  FormLabel,
  Input,
  VStack,
  Button,
  Text,
  FormHelperText
} from "@chakra-ui/react";
import { AuthenticationAPI } from "../api/endpoints/authentication";
import { Footer } from "../components/Footer";
import { FE_BASE_URL } from "../api/constant";
import { deleteToken, redirectToLogin } from "../api/helper";
import Link from "next/link"

const SignUp = () => {
  const [npm, setNpm] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("")

  let auth = new AuthenticationAPI()

  const onSubmit = () => {
    deleteToken()
    auth.signUp({ npm: npm, email: email, password: password })
      .then((res) => {
        redirectToLogin()
      })
      .catch((err) => setErrorMessage("Email sudah terdaftar"))
  };

  return (
    <>
      <Center backgroundColor="teal.400" minHeight="100vh">
        <VStack
          backgroundColor="teal.50"
          py="32px"
          px="100px"
          borderRadius="10px"
          spacing={8}
          minW={{ md: "600px" }}
        >
          <Heading>Sign Up</Heading>
          <FormControl id="email" isRequired>
            <FormLabel>Email</FormLabel>
            <Input
              backgroundColor="white"
              type="email"
              placeholder="dummy@dummy.com"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <FormHelperText color="red.500">{errorMessage}</FormHelperText>
          </FormControl>
          <FormControl id="npm" isRequired>
            <FormLabel>NPM</FormLabel>
            <Input
              backgroundColor="white"
              type="text"
              placeholder="1234567890"
              value={npm}
              onChange={(e) => setNpm(e.target.value)}
            />
          </FormControl>
          <FormControl id="password" isRequired>
            <FormLabel>Password</FormLabel>
            <Input
              backgroundColor="white"
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormControl>
          <Button backgroundColor="blue.900" color="white" onClick={() => onSubmit()}>
            Sign Up
        </Button>
          <Text>Already have have an account ? <Link href="/login"><span style={{ textDecoration: "underline", cursor: "pointer" }}>Sign In</span></Link></Text>
        </VStack>

      </Center>
      <Footer />
    </>
  );
};

export default SignUp;
