import "../styles/globals.scss";
import { ChakraProvider } from "@chakra-ui/react";
import axios from "axios";
import { useEffect } from "react";
import { getToken } from "../api/helper";
import { useRouter } from "next/router";

const PUBLIC_ROUTE = ["/", "/signup", "/login"];

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
  );
}

export default MyApp;
